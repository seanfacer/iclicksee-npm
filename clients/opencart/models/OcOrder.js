/**
	Generated by sails-inverse-model
	Date:Fri Mar 01 2019 16:33:42 GMT+0000 (Coordinated Universal Time)
*/

module.exports = {
    attributes: {
        order_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            required: true
        },
        invoice_no: {
            type: "number",
            columnType: "int",
            isInteger: true,
            defaultsTo: 0
        },
        invoice_prefix: {
            type: "string",
            columnType: "varchar",
            maxLength: 26,
            required: true
        },
        store_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            defaultsTo: 0
        },
        store_name: {
            type: "string",
            columnType: "varchar",
            maxLength: 64,
            required: true
        },
        store_url: {
            type: "string",
            columnType: "varchar",
            maxLength: 255,
            required: true
        },
        customer_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            defaultsTo: 0
        },
        customer_group_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            defaultsTo: 0
        },
        firstname: {
            type: "string",
            columnType: "varchar",
            maxLength: 32,
            required: true
        },
        lastname: {
            type: "string",
            columnType: "varchar",
            maxLength: 32,
            required: true
        },
        email: {
            type: "string",
            columnType: "varchar",
            maxLength: 96,
            required: true
        },
        telephone: {
            type: "string",
            columnType: "varchar",
            maxLength: 32,
            required: true
        },
        fax: {
            type: "string",
            columnType: "varchar",
            maxLength: 32,
            required: true
        },
        custom_field: {
            type: "string",
            columnType: "text",
            required: true
        },
        payment_firstname: {
            type: "string",
            columnType: "varchar",
            maxLength: 32,
            required: true
        },
        payment_lastname: {
            type: "string",
            columnType: "varchar",
            maxLength: 32,
            required: true
        },
        payment_company: {
            type: "string",
            columnType: "varchar",
            maxLength: 60,
            required: true
        },
        payment_address_1: {
            type: "string",
            columnType: "varchar",
            maxLength: 128,
            required: true
        },
        payment_address_2: {
            type: "string",
            columnType: "varchar",
            maxLength: 128,
            required: true
        },
        payment_city: {
            type: "string",
            columnType: "varchar",
            maxLength: 128,
            required: true
        },
        payment_postcode: {
            type: "string",
            columnType: "varchar",
            maxLength: 10,
            required: true
        },
        payment_country: {
            type: "string",
            columnType: "varchar",
            maxLength: 128,
            required: true
        },
        payment_country_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            required: true
        },
        payment_zone: {
            type: "string",
            columnType: "varchar",
            maxLength: 128,
            required: true
        },
        payment_zone_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            required: true
        },
        payment_address_format: {
            type: "string",
            columnType: "text",
            required: true
        },
        payment_custom_field: {
            type: "string",
            columnType: "text",
            required: true
        },
        payment_method: {
            type: "string",
            columnType: "varchar",
            maxLength: 128,
            required: true
        },
        payment_code: {
            type: "string",
            columnType: "varchar",
            maxLength: 128,
            required: true
        },
        shipping_firstname: {
            type: "string",
            columnType: "varchar",
            maxLength: 32,
            required: true
        },
        shipping_lastname: {
            type: "string",
            columnType: "varchar",
            maxLength: 32,
            required: true
        },
        shipping_company: {
            type: "string",
            columnType: "varchar",
            maxLength: 40,
            required: true
        },
        shipping_address_1: {
            type: "string",
            columnType: "varchar",
            maxLength: 128,
            required: true
        },
        shipping_address_2: {
            type: "string",
            columnType: "varchar",
            maxLength: 128,
            required: true
        },
        shipping_city: {
            type: "string",
            columnType: "varchar",
            maxLength: 128,
            required: true
        },
        shipping_postcode: {
            type: "string",
            columnType: "varchar",
            maxLength: 10,
            required: true
        },
        shipping_country: {
            type: "string",
            columnType: "varchar",
            maxLength: 128,
            required: true
        },
        shipping_country_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            required: true
        },
        shipping_zone: {
            type: "string",
            columnType: "varchar",
            maxLength: 128,
            required: true
        },
        shipping_zone_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            required: true
        },
        shipping_address_format: {
            type: "string",
            columnType: "text",
            required: true
        },
        shipping_custom_field: {
            type: "string",
            columnType: "text",
            required: true
        },
        shipping_method: {
            type: "string",
            columnType: "varchar",
            maxLength: 128,
            required: true
        },
        shipping_code: {
            type: "string",
            columnType: "varchar",
            maxLength: 128,
            required: true
        },
        comment: {
            type: "string",
            columnType: "text",
            required: true
        },
        total: {
            type: "number",
            columnType: "decimal",
            defaultsTo: 0.0000
        },
        order_status_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            defaultsTo: 0
        },
        affiliate_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            required: true
        },
        commission: {
            type: "number",
            columnType: "decimal",
            required: true
        },
        marketing_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            required: true
        },
        tracking: {
            type: "string",
            columnType: "varchar",
            maxLength: 64,
            required: true
        },
        language_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            required: true
        },
        currency_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            required: true
        },
        currency_code: {
            type: "string",
            columnType: "varchar",
            maxLength: 3,
            required: true
        },
        currency_value: {
            type: "number",
            columnType: "decimal",
            defaultsTo: 1.00000000
        },
        ip: {
            type: "string",
            columnType: "varchar",
            maxLength: 40,
            required: true
        },
        forwarded_ip: {
            type: "string",
            columnType: "varchar",
            maxLength: 40,
            required: true
        },
        user_agent: {
            type: "string",
            columnType: "varchar",
            maxLength: 255,
            required: true
        },
        accept_language: {
            type: "string",
            columnType: "varchar",
            maxLength: 255,
            required: true
        },
        date_added: {
            type: "string",
            columnType: "datetime",
            required: true
        },
        date_modified: {
            type: "string",
            columnType: "datetime",
            required: true
        }
    }
};