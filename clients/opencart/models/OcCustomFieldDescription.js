/**
	Generated by sails-inverse-model
	Date:Fri Mar 01 2019 16:33:42 GMT+0000 (Coordinated Universal Time)
*/

module.exports = {
    attributes: {
        custom_field_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            required: true
        },
        language_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            required: true
        },
        name: {
            type: "string",
            columnType: "varchar",
            maxLength: 128,
            required: true
        }
    }
};