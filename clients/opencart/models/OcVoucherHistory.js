/**
	Generated by sails-inverse-model
	Date:Fri Mar 01 2019 16:33:42 GMT+0000 (Coordinated Universal Time)
*/

module.exports = {
    attributes: {
        voucher_history_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            required: true
        },
        voucher_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            required: true
        },
        order_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            required: true
        },
        amount: {
            type: "number",
            columnType: "decimal",
            required: true
        },
        date_added: {
            type: "string",
            columnType: "datetime",
            required: true
        }
    }
};