/**
	Generated by sails-inverse-model
	Date:Fri Mar 01 2019 16:33:42 GMT+0000 (Coordinated Universal Time)
*/

module.exports = {
    attributes: {
        voucher_theme_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            required: true
        },
        image: {
            type: "string",
            columnType: "varchar",
            maxLength: 255,
            required: true
        }
    }
};