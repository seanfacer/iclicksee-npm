/**
	Generated by sails-inverse-model
	Date:Fri Mar 01 2019 16:33:42 GMT+0000 (Coordinated Universal Time)
*/

module.exports = {
    attributes: {
        voucher_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            required: true
        },
        order_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            required: true
        },
        code: {
            type: "string",
            columnType: "varchar",
            maxLength: 10,
            required: true
        },
        from_name: {
            type: "string",
            columnType: "varchar",
            maxLength: 64,
            required: true
        },
        from_email: {
            type: "string",
            columnType: "varchar",
            maxLength: 96,
            required: true
        },
        to_name: {
            type: "string",
            columnType: "varchar",
            maxLength: 64,
            required: true
        },
        to_email: {
            type: "string",
            columnType: "varchar",
            maxLength: 96,
            required: true
        },
        voucher_theme_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            required: true
        },
        message: {
            type: "string",
            columnType: "text",
            required: true
        },
        amount: {
            type: "number",
            columnType: "decimal",
            required: true
        },
        status: {
            type: "number",
            columnType: "tinyint",
            isInteger: true,
            required: true
        },
        date_added: {
            type: "string",
            columnType: "datetime",
            required: true
        }
    }
};