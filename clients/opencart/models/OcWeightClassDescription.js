/**
	Generated by sails-inverse-model
	Date:Fri Mar 01 2019 16:33:42 GMT+0000 (Coordinated Universal Time)
*/

module.exports = {
    attributes: {
        weight_class_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            required: true
        },
        language_id: {
            type: "number",
            columnType: "int",
            isInteger: true,
            required: true
        },
        title: {
            type: "string",
            columnType: "varchar",
            maxLength: 32,
            required: true
        },
        unit: {
            type: "string",
            columnType: "varchar",
            maxLength: 4,
            required: true
        }
    }
};