
var moment = require('moment');

module.exports = {
  friendlyName: 'Create',
  description: 'Creates the Opencart Product and all relational data.',
  exits: {
    success: {
      description: 'Product Created.'
    },
    error: {
      description: 'Unable to create product.'
    },
  },
  fn: async function (product, companyName) {
    var ocProduct = {
      model: product.model,
      viewed : product.viewed ? product.viewed : 0,
      image : product.image_dest,
      //Still to do
      shipping: product.shipping,
      manufacturer_id: product.manufacturer_id,
      //Add a check to see if this is a valid mpn/sku/ean/etc (character count etc);
      //Using suppliers sku if its not a product we supply or have an sku for
      sku: product.sku ? product.sku : '',
      upc: product.upc ? product.upc : '',
      ean: product.ean ? product.ean : '',
      jan: product.jan ? product.upc : '',
      isbn: product.isbn ? product.isbn : '',
      mpn: product.mpn ? product.mpn : '',
      //Add a check to see if this is a valid mpn/sku/ean/etc (character count etc);
      price: product.price,
      length: product.length ? prodcut.length : '',
      width: product.width ? prodcut.width : '',
      height: product.height ? prodcut.height : '',
      //End still to do
      location: product.location ? product.location: '',
      quantity: product.stock_level,
      minimum: product.minimum ? product.minimum : '1',
      subtract: product.subtract ? product.subtract : '1',
      weight: product.weight,
      stock_status_id: product.stock_status_id,
      points: product.points ? product.points : '0',
      sort_order: product.sort_order ? product.sort_order : '',
      weight_class_id: product.weight_class_id ? product.weight_class_id : '',
      length_class_id: product.length_class_id ? product.length_class_id : '',
      status: product.status,
      tax_class_id: product.tax_class_id,
      date_available: moment.utc().format('Y-MM-DD HH:mm:ss'),
      date_added: moment.utc().format('Y-MM-DD HH:mm:ss'),
      date_modified: moment.utc().format('Y-MM-DD HH:mm:ss'),
      ocProductDescription: {
        language_id: product.ocProductDescription.language_id,
        name: product.name,
        description: product.description,
        tag: 'Buy Product '+ product.name,
        meta_title: product.meta_title,
        meta_description: 'Buy Product '+product.name+ ' ' + companyName,
        meta_keyword: product.meta_keyword
      },
      ocProductToLayout: {
        store_id: product.ocProductToLayout.store_id ? product.ocProductToLayout.store_id : '0',
        layout_id: product.ocProductToLayout.layout_id ? product.ocProductToLayout.layout_id : '1'
      },
      ocSeoUrl: {
        store_id: product.ocProductToLayout.store_id ? product.ocProductToLayout.store_id : '0',
        layout_id: product.ocProductToLayout.layout_id ? product.ocProductToLayout.layout_id : '1',
        keyword: product.meta_keyword
      },
      ocProductToCategory: {
        category_id: product.category.opencart_category_id
      },
      ocProductToStore: {
        store_id: product.ocProductToLayout.store_id ? product.ocProductToLayout.store_id : '0',
      }
    }
    return ocProduct;
  }
};
