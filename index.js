// Convenience file to require the SDK from the root of the repository
module.exports = require('./lib/iclicksee');

var Waterline = require('waterline');
var sailMysql = require('sails-mysql');
var waterline = new Waterline();
var ocProduct = require('./clients/opencart/models/ocProduct');
waterline.registerModel(ocProduct);

var config = require('./config/datastores');

console.log(config.datastores.default);

waterline.initialize(config, (err, ontology)=>{
  if (err) {
    console.error(err);
    return;
  }

  // Tease out fully initialized models.
  var User = ontology.collections.user;
  var Pet = ontology.collections.pet;

  // Since we're using `await`, we'll scope our selves an async IIFE:
  (async ()=>{
    // First we create a user
    var user = await User.create({
      firstName: 'Neil',
      lastName: 'Armstrong'
    });

    // Then we create the pet
    var pet = await Pet.create({
      breed: 'beagle',
      type: 'dog',
      name: 'Astro',
      owner: user.id
    });

    // Then we grab all users and their pets
    var users = await User.find().populate('pets');
    console.log(users);
  })()
  .then(()=>{
    // All done.
  })
  .catch((err)=>{
    console.error(err);
  });//_∏_

});
